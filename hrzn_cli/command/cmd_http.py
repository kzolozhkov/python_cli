import click
from httpie.core import main as httpie_main
from itertools import dropwhile
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

@click.command(add_help_option=False, context_settings=dict(
    ignore_unknown_options=True
))
@click.pass_obj
@click.argument("non_interactive_args", nargs=-1, required=False)
def http(hrzn_ctx, non_interactive_args):
    """Embedded HTTPIE """
    if "--help" in non_interactive_args:
        httpie_main(args=["--help"])
        return

    method_idx = next(
        dropwhile(lambda el: el[1] not in ["GET", "PUT", "POST", "DELETE"], enumerate(non_interactive_args)), None)
    if method_idx is not None:
        final_args = []
        final_args.extend(non_interactive_args[:method_idx[0]])
        final_args.append("--auth={}".format(hrzn_ctx.auth.token))
        final_args.append("--auth-type=jwt")
        final_args.append(method_idx[1])
        final_args.append("{}/{}".format(hrzn_ctx.api_url, non_interactive_args[method_idx[0] + 1]))
        final_args.extend(non_interactive_args[method_idx[0] + 2:])

        logger.debug("HTTPIE arguments", final_args)
        try:
            httpie_main(args=final_args)
        finally:
            pass
