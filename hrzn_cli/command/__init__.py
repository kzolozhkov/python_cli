import importlib
import pkgutil

from .root import root_cmd

cmd_modules = [m.name for m in pkgutil.iter_modules(path=__path__, prefix=__name__ + ".") if "cmd_" in m.name]
for mname in cmd_modules:
    cmd = getattr(importlib.import_module(mname),
                  mname.replace(__name__, "").replace(".cmd_", ""))
    root_cmd.add_command(cmd)
