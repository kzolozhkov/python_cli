from click import Group
from click import command
from click.decorators import option


class HrznGroup(Group):
    def append_default_options(self, f):
        f = option("--json", "raw_json", help="Output as raw JSON", is_flag=True, default=False)(f)
        return f

    def command(self, *args, **kwargs):
        def decorator(f):
            cmd = command(*args, **kwargs)(self.append_default_options(f))
            self.add_command(cmd)
            return cmd

        return decorator
