import click
from requests.exceptions import HTTPError
from tabulate import tabulate

from prompt_toolkit import prompt
from hrzn_cli.command.base import HrznGroup
from hrzn_cli.format import *


def format_list(data):
    data = [[format_uuid('PLAYER', op['playerUUID']),
             op['email'], op.get('firstName', '') + ' ' + op.get('lastName', ''),
             op['registrationDate']] for op in data["content"]]

    return tabulate(data, tablefmt='fancy_igrid', headers=['UUID', 'Email', 'Full Name', 'Signup Date'])


def format_single(data):
    return tabulate([
        ["UUID", format_uuid("PLAYER", data["playerUUID"])],
        ["First Name", data["firstName"]],
        ["Last Name", data["lastName"]],
        ["Email", data["email"]],
        ["Balance", format_amount(data["balance"])],
    ])


@click.group(cls=HrznGroup)
def player():
    """Do smth with players"""
    pass


@player.command()
@click.pass_obj
@click.argument("keyword", type=click.STRING, required=False)
def find(hrzn_ctx, raw_json, keyword):
    """ Search players by KEYWORD"""
    try:
        res = hrzn_ctx.get("profile/profiles", params={'searchValue': keyword})
    except HTTPError as e:
        if e.response.status_code == 404:
            res = {"totalElements": 0}
        else:
            raise e

    if res["totalElements"] == 0:
        print("No players found")
    elif res["totalElements"] > 1:
        formatted_output(res, raw_json, format_list)
    else:
        res = hrzn_ctx.get("profile/profiles/{}".format(res["content"][0]["playerUUID"]))
        formatted_output(res, raw_json, format_single)


@player.command()
@click.pass_obj
@click.argument("uuid", type=click.STRING, required=True)
@click.option('--duration', "-d", "duration", type=click.INT, default=5,
              help='Duration of reality check in minutes')
@click.option('--response', "-r", "response", type=click.INT, default=5,
              help='Duration of waiting for player response in minutes')
def reality_check(hrzn_ctx, raw_json, duration, response, uuid):

    request = {'playerUUID': uuid, 'duration': duration, 'durationTimeUnit': 'MINUTES',
               'playerResponsePeriod': response, 'playerResponsePeriodTimeUnit': 'MINUTES'}

    resp = hrzn_ctx.post('playing-session/reality-check', json=request)

    print("Reality check result is {}".format(resp["message"]))

@player.command()
@click.pass_obj
@click.argument("keyword", type=click.STRING, required=False)
@click.option('--reason', "-r", "reason", type=click.STRING, default="PLAYER_PROFILE.PROFILE.BLOCK_REASONS.DUPLICATE_ACCOUNT")
@click.option('--comment', "-c", "comment", type=click.STRING, default="Batch block")
def block(hrzn_ctx, raw_json, keyword, reason, comment):
    """ Block players by KEYWORD"""
    try:
        res = hrzn_ctx.get("profile/profiles", params={'searchValue': keyword})
    except HTTPError as e:
        if e.response.status_code == 404:
            res = {"totalElements": 0}
        else:
            raise e

    if res["totalElements"] == 0:
        print("No players found")
        return
    
    if prompt("{} Players matched, do you want to proceed ? [Y/n] ".format(res["totalElements"])) == 'Y':
        blocked = 0
        failed = 0

        for p in range(0, res["totalPages"]):
            if p > 0: # No need to fetch the first page again
                res = hrzn_ctx.get("profile/profiles", params={'searchValue': keyword, 'page': p})
            
            for player in res["content"]:
                try:
                    # Unverified accounts cannot be blocked at the moment 
                    # so a workaround is required
                    hrzn_ctx.post("profile/verification/{}".format(player["playerUUID"]))
                    hrzn_ctx.put("profile/profiles/{}/block".format(player["playerUUID"]), json={'reason': reason, 'comment': comment})
                    blocked += 1
                except HTTPError as e:
                    failed += 1
            p += 1
            
        print("{} Players blocked, {} failed".format(blocked, failed))

    