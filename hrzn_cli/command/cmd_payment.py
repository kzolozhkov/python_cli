import click
from tabulate import tabulate

from hrzn_cli.command.base import HrznGroup
from hrzn_cli.format import format_amount, format_uuid, formatted_output


def format_list(data):
    data = [[format_uuid("PAYMENT", p['paymentId']),
             format_uuid("PLAYER", p['player']['playerUUID']),
             p["paymentType"],
             format_amount(p["amount"])] for p in data['content']]
    return tabulate(data, tablefmt='fancy_igrid', headers=['PaymentId', "Player UUID", "Type", "Amount"])


@click.group(cls=HrznGroup)
def payment():
    """Do smth with payments"""
    pass


@payment.command()
@click.pass_obj
@click.argument("keyword", type=click.STRING)
def find(hrzn_ctx, raw_json, keyword):
    res = hrzn_ctx.get("payment/payments", params={'keyword': keyword})

    if res["totalElements"] == 0:
        print("No payments found")
    else:
        formatted_output(res, raw_json, format_list)
