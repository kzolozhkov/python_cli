import click


@click.group(invoke_without_command=True)
def root_cmd():
    pass


@root_cmd.command()
@click.pass_obj
def exit(hrzn_ctx):
    """Exit shell"""
    hrzn_ctx.should_exit = True


@root_cmd.command()
@click.pass_obj
def quit(hrzn_ctx):
    """Exit shell"""
    hrzn_ctx.should_exit = True


@root_cmd.command()
@click.pass_context
def help(ctx):
    """Display help"""
    print(root_cmd.get_help(ctx))


@root_cmd.command()
@click.pass_context
def clear(ctx):
    """Clear screen"""
    click.clear()
