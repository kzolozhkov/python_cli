import click
from tabulate import tabulate

from hrzn_cli.command.base import HrznGroup
from hrzn_cli.format import formatted_output


def format_single(data):
    return tabulate([
        ["UUID", data["uuid"]],
        ["Brand", data["brandId"]],
        ["Date", data["publicationDate"]],
        ["Ref", data["externalRef"]]
    ])


@click.group(cls=HrznGroup)
def terms():
    """Do smth with terms and conditions"""
    pass


@terms.command()
@click.pass_obj
@click.option("-d", "--draft", "draft", is_flag=True, default=False)
@click.option("-r", "--reference", "ref", type=click.STRING, default=None)
@click.option("-b", "--brand", "brand", type=click.STRING)
def update(hrzn_ctx, raw_json, draft, ref, brand):
    """ Publish new T&C"""

    req = {"brandId": brand if brand is not None else hrzn_ctx.brand, "draft": draft, "externalRef": ref}

    res = hrzn_ctx.post("profile/terms_and_conditions", json=req)

    if "uuid" in res:
        print("Published new T&C")
        formatted_output(res, raw_json, format_single)


@terms.command()
@click.pass_obj
@click.option("-b", "--brand", "brand", type=click.STRING)
def last(hrzn_ctx, raw_json, brand):
    "Retrieve last T&C"
    res = hrzn_ctx.get("profile/public/terms_and_conditions",
                       {'brandId': brand} if brand is not None else {'brandId': hrzn_ctx.brand})
    formatted_output(res, raw_json, format_single)
