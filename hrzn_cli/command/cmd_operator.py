import logging

import click
import time
from tabulate import tabulate

from hrzn_cli import DEPARTMENTS
from hrzn_cli.command.base import HrznGroup
from hrzn_cli.format import *

logger = logging.getLogger(__name__)


def format_list(data):
    data = [[format_uuid("OPERATOR", op['uuid']), op['email'], op["operatorStatus"]] for op in data["content"]]
    return tabulate(data, tablefmt='fancy_igrid', headers=["UUID", "Email", "Status"])


def format_one(data):
    return tabulate([
        ["UUID", format_uuid("OPERATOR", data["uuid"])],
        ["First Name", data["firstName"]],
        ["Last Name", data["lastName"]],
        ["Email", data["email"]],
    ])


@click.group(cls=HrznGroup)
def operator():
    """Do smth with operators"""
    pass


@operator.command()
@click.pass_obj
@click.argument("keyword", type=click.STRING, required=False)
@click.option('--status', "-s", "status", type=click.Choice(['INACTIVE', 'ACTIVE', "CLOSED"]),
              default=None, help='Status filter')
def find(hrzn_ctx, keyword, raw_json, status):
    """Search operators by KEYWORD and additional options"""

    params = {'searchBy': keyword}
    if status is not None:
        params["status"] = status

    res = hrzn_ctx.get("operator/operators", params=params)

    if res["totalElements"] == 0:
        print("No operators found")
    elif res["totalElements"] > 1:
        formatted_output(res, raw_json, format_list)
    else:
        res = hrzn_ctx.get("operator/operators/{}".format(res["content"][0]["uuid"]))
        formatted_output(res, raw_json, format_one)


def add_authority(hrzn_ctx, uuid, department, role):
    hrzn_ctx.post2("auth/credentials/" + uuid + "/authorities", json={
        "role": role, "department": department, "brandId": hrzn_ctx.brand
    })


@operator.command()
@click.pass_obj
@click.argument("uuid", type=click.STRING, required=True)
@click.argument("authority", type=click.STRING, nargs=-1)
def auth(hrzn_ctx, raw_json, uuid, authority):
    """ Add or remove AUTHORITY from operator,

When prefixed with `+' add authority, if prefixed with `-' remove it.
Example: +CS:ROLE -RFP:ROLE2
    """
    for a in authority:
        if a.startswith("+"):
            (department, role) = a[1:].split(":")
            add_authority(hrzn_ctx, uuid, department, role)
            pass
        elif a.startswith("-"):
            pass
    pass


@operator.command()
@click.pass_obj
@click.option("-F", "--firstname", "fname", type=click.STRING, default=None, help="First name")
@click.option("-L", "--lastname", "lname", type=click.STRING, default=None, help="Last name")
@click.option("-P", "--password", "password", type=click.STRING, default=None,
              help="Password, if provided - email won't be sent, operator activated immediately")
@click.argument("email", type=click.STRING)
@click.argument("department", type=click.Choice(DEPARTMENTS))
@click.argument("role", type=click.STRING)
def add(hrzn_ctx, raw_json, fname, lname, password, email, department, role):
    """ Register new operator, activation link will be sent to EMAIL"""

    res = hrzn_ctx.post("operator/operators", json={
        'brandId': hrzn_ctx.brand,
        'email': email,
        'firstName': fname if fname is not None else email,
        'lastName': lname if lname is not None else email,
        'sendMail': True if password is None else False
    })

    if "uuid" in res and password is None:
        time.sleep(5)
        add_authority(hrzn_ctx, res['uuid'], department, role)

        print("Operator registered, UUID:", res['uuid'])
        print("Please check", email, "for setting a password link")
    elif "uuid" in res and password is not None:
        token = hrzn_ctx.get_text("operator/operators/reset-token", params={"operatorUUID": res["uuid"]})
        res = hrzn_ctx.post("operator/public/operators/activate?brandId=" + hrzn_ctx.brand, json={
            "password": password, "token": token
        })
        if "uuid" in res:
            time.sleep(5)
            add_authority(hrzn_ctx, res['uuid'], department, role)
            print("Operator registered, UUID:", res['uuid'])
