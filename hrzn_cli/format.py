import json

from httpie.output.formatters.colors import Solarized256Style
from pygments import highlight
from pygments.formatters.terminal256 import Terminal256Formatter
from pygments.lexers.data import JsonLexer


def format_amount(raw_amount):
    return "{} {}".format(raw_amount["amount"],
                          raw_amount["currency"]) if "amount" in raw_amount and "currency" in raw_amount else "N/A"


def format_uuid(prefix, raw_uuid):
    tmp = raw_uuid.replace(prefix, "")[:9]
    return ("{}{}" if tmp.startswith("-") else "{}-{}").format(prefix[:2], tmp)


def formatted_output(data, raw_json, formatter):
    if not raw_json:
        print(formatter(data))
    else:
        print(highlight(json.dumps(data, indent=2, sort_keys=True), JsonLexer(), Terminal256Formatter(style=Solarized256Style)))
