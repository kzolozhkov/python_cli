from collections import namedtuple


def jwt_auth(self, r):
    r.headers['Authorization'] = 'Bearer {}'.format(self.token)
    return r


HrznAuth = namedtuple("HrznAuth", ["token", "login", "department"])
HrznAuth.__call__ = jwt_auth


def signin(hrzn_ctx):
    r = hrzn_ctx.post('auth/signin', json={'login': hrzn_ctx.login,
                                           'password': hrzn_ctx.password,
                                           'department': hrzn_ctx.department,
                                           'brandId': hrzn_ctx.brand})

    hrzn_ctx.auth = HrznAuth(r['token'], hrzn_ctx.login, hrzn_ctx.department)
    return hrzn_ctx
