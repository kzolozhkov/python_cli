import requests
from pygments.token import Token


class HrznContext():
    def __init__(self, config):
        self.should_exit = False
        self._config = config
        self.auth = None

    @property
    def brand(self):
        return self._config['brand']

    @property
    def api_url(self):
        return self._config.get('api_url')

    @property
    def login(self):
        return self._config.get('login')

    @property
    def password(self):
        return self._config.get('password')

    @property
    def department(self):
        return self._config.get('department')

    def post(self, api, json=None):
        res_url = '{}/{}'.format(self.api_url, api)
        r = requests.post(res_url, auth=self.auth, json=json)
        r.raise_for_status()
        return r.json()

    def post2(self, api, json=None):
        res_url = '{}/{}'.format(self.api_url, api)
        r = requests.post(res_url, auth=self.auth, json=json)
        r.raise_for_status()

    def put(self, api, json=None):
        res_url = '{}/{}'.format(self.api_url, api)
        r = requests.put(res_url, auth=self.auth, json=json)
        r.raise_for_status()
        return r.json()

    def get(self, api, params=None):
        res_url = '{}/{}'.format(self.api_url, api)
        r = requests.get(res_url, auth=self.auth, params=params)
        r.raise_for_status()
        return r.json()

    def get_text(self, api, params=None):
        res_url = '{}/{}'.format(self.api_url, api)
        r = requests.get(res_url, auth=self.auth, params=params)
        r.raise_for_status()
        return r.text

    def get_rprompt_tokens(self, cli):
        return [
#            (Token.Whitespace, ' '),
#            (Token.RPrompt, '   This is sooo great   '),
        ]

    def prompt_tokens(self, _):
        return [
            (Token.Whitespace, "\n"),
            (Token.Keyword, self.login),
            (Token.Whitespace, " @ "),
            (Token.Keyword, self.brand),
            (Token.ApiUrl, " ({})".format(self.api_url)),
            (Token.Whitespaсe, "\n"),
            (Token.Prompt, "$$$"),
            (Token.Whitespace, ' '),
        ]
