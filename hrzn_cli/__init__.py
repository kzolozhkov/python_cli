import logging

import os

DEPARTMENTS = ['ADMINISTRATION', 'CS', 'RFP', 'MARKETING', 'BI', 'E2E']

logging.basicConfig(level=logging.WARN, filename=os.path.expanduser('~/.hrzn_cli.log'), filemode='w',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logging.getLogger(__name__).setLevel(logging.DEBUG)
