import logging
from typing import List

import click
from click.exceptions import UsageError
from requests.exceptions import HTTPError

from .command.root import root_cmd

logger = logging.getLogger(__name__)


def execute(hrzn_ctx, command_line: List):
    click_ctx = click.Context(root_cmd, parent=None,
                              obj=hrzn_ctx, info_name="hrzn-cli",
                              allow_extra_args=True, allow_interspersed_args=False)

    args = command_line
    if len(args) > 0:
        with click_ctx.scope():
            try:
                root_cmd.parse_args(click_ctx, args)
                root_cmd.invoke(click_ctx)
            except UsageError as ex:
                logger.info('Usage error executing `%s`', " ".join(command_line), exc_info=True)
                ex.show()
            except HTTPError as ex:
                logger.info('Http error executing `%s`\n--------------\n%s\n---------------', " ".join(command_line),
                            ex.response.text, exc_info=True)
                print("Http error code", ex.response.status_code)
            except SystemExit:
                logger.info("SystemExit", exc_info=True)
