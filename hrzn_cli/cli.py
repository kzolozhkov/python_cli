import click
import configparser
import os
from prompt_toolkit import prompt
from prompt_toolkit.contrib.completers import WordCompleter
from prompt_toolkit.history import InMemoryHistory
from prompt_toolkit.styles.from_dict import style_from_dict
from pygments.token import Token

from hrzn_cli.completion import HrznCliCompleter
from . import DEPARTMENTS
from .auth import signin
from .context import HrznContext
from .execution import execute, root_cmd

prompt_style = style_from_dict({
    Token.Keyword: '#ffff00',
    Token.ApiUrl:  '#00e000',
    Token.Prompt:  '#00e000 bold',
    Token.RPrompt: 'bg:#ff0066 #ffffff',
})


def ask_store_rc(parser, message):
    if prompt("{} ~/.hrznrc ? [Y/n] ".format(message)) == 'Y':
        with open(os.path.expanduser('~/.hrznrc'), 'w') as f:
            parser.write(f)


@click.command(add_help_option=True, context_settings=dict(ignore_unknown_options=True))
@click.option("--destination", "-d", "destination", type=str)
@click.argument("non_interactive_args", nargs=-1, required=False)
def hrzn_cli(destination, non_interactive_args):
    parser = configparser.ConfigParser()

    try:
        with open(os.path.expanduser('~/.hrznrc'), 'r') as f:
            parser.read_file(f)
    except IOError:
        ask_store_rc(parser, "Do you want to create")

    if destination is None:
        destination = prompt("What destination ?: ", completer=WordCompleter(parser.sections()))
        print("\u001b[2A")

    if destination not in parser:
        print("Missing configuration for `{}` in ~/.hrznrc, please provide following".format(destination))

        parser[destination] = {
            "api_url": prompt("API Url: "),
            "login": prompt("Login: "),
            "password": prompt("Password: ", is_password=True),
            "brand": prompt("Brand: "),
            "department": prompt("Department: ", completer=WordCompleter(DEPARTMENTS))
        }

        ask_store_rc(parser, "Do you want to save `{}` configuration to".format(destination))
        print("\u001b[7A")

    hrzn_ctx = signin(HrznContext(parser[destination]))

    if len(non_interactive_args) == 0:
        history = InMemoryHistory()
        while True:
            try:
                text = prompt(get_prompt_tokens=hrzn_ctx.prompt_tokens,
                              get_rprompt_tokens=hrzn_ctx.get_rprompt_tokens,
                              style=prompt_style, history=history,
                              completer=HrznCliCompleter(root_cmd), complete_while_typing=False)
            except (EOFError, KeyboardInterrupt):
                break
            else:
                execute(hrzn_ctx, text.strip().split())
                if hrzn_ctx.should_exit:
                    break

        print('Goodbye !')
    else:
        execute(hrzn_ctx, list(non_interactive_args))


if __name__ == '__main__':
    hrzn_cli()
