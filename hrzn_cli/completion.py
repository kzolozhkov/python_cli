import logging

import traceback
from click import Option
from parsimonious.exceptions import ParseError
from parsimonious.grammar import Grammar
from parsimonious.nodes import NodeVisitor
from prompt_toolkit.completion import Completer, Completion

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

grammar = Grammar(r""" 
    root            = _? ( cmd_sub_opts / cmd_sub / cmd )

    cmd_sub_opts    = word _ word _ ( (option_value / option ) _? )* 
    cmd_sub         = word _ word?
    cmd             = word?   

    option_value    = ~"-{1,2}" word _ word?
    option          = ~"-{1,2}" word?

    word            = ~"[^\s]+"
    _               = ~"\s+"    
""")


class CompletionVisitor(NodeVisitor):
    def __init__(self, root_cmd):
        self._suggestions = None
        self._root_cmd = root_cmd

    @property
    def suggestions(self):
        return self._suggestions

    def visit_cmd_sub_opts(self, node, children):
        logger.debug("visit_cmd_sub_opts subCmd=%s subCmd2=%s", children[0].text, children[2].text)

        sub_cmd = self._root_cmd.commands.get(children[0].text)
        if sub_cmd is not None and hasattr(sub_cmd, "commands"):
            sub_cmd2 = sub_cmd.commands.get(children[2].text)
            if sub_cmd2 is not None and hasattr(sub_cmd2, "params"):
                options = [o for o in sub_cmd2.params if isinstance(o, Option)]
                arr = []
                for o in options:
                    if all(opt_name not in node.text for opt_name in o.opts):
                        arr.extend((opt_name, o.help) for opt_name in o.opts)

                if "--help" not in node.text:
                    arr.append(("--help", "Display help"))

                self._suggestions = arr

        return node

    def visit_cmd(self, node, _):
        if self._suggestions is None:
            self._suggestions = [(cmd_name, cmd.short_help) for cmd_name, cmd in self._root_cmd.commands.items()]
        return node

    def visit_cmd_sub(self, node, children):
        if self._suggestions is None:
            sub_cmd = self._root_cmd.commands.get(children[0].text)

            arr = [(cmd_name, cmd.short_help) for cmd_name, cmd in
                   (sub_cmd.commands if sub_cmd is not None and hasattr(sub_cmd, "commands") else {}).items()]

            arr.append(("--help", "Display help"))

            self._suggestions = arr
        return node

    def generic_visit(self, node, _):
        logger.debug("generic_visit %s", self._suggestions)
        return node


class HrznCliCompleter(Completer):
    def __init__(self, root_cmd):
        self._root_cmd = root_cmd

    def get_completions(self, document, _):
        cmd = document.text

        try:
            root = grammar.parse(cmd)
        except ParseError:
            logger.debug(traceback.format_exc())
            yield from ()
        else:
            visitor = CompletionVisitor(self._root_cmd)
            visitor.visit(root)

            cur_word = document.get_word_under_cursor()
            prev_word = document.get_word_before_cursor()
            prev_word_end = document.find_previous_word_ending()

            pos = -len(cur_word) if cur_word != "" else (prev_word_end if prev_word_end is not None else 0)

            logger.debug("[%s] cur=[%s] prev=[%s] prev_end=[%s] [%s]", cmd, cur_word,
                         prev_word, prev_word_end, pos)

            if visitor.suggestions is not None:
                for s in visitor.suggestions:
                    logger.debug("sugg=[%s] prev_word=[%s]", s, prev_word)
                    if isinstance(s, tuple) and s[0].startswith(prev_word):
                        yield Completion(s[0], pos, display_meta=s[1])
                    elif isinstance(s, str) and s.startswith(prev_word):
                        yield Completion(s, pos)
            else:
                yield from ()
