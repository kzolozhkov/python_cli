import click
import logging
import yaml
from kazoo.client import KazooClient

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)


def save_to_zk(zk_url, root_path, is_rm, data):
    zk = KazooClient(hosts=zk_url)
    zk.start()

    if is_rm:
        logging.info("Deleting %s", root_path)
        zk.delete(root_path, recursive=True)

    for k, v in data:
        key_path = "{}/{}".format(root_path, k)
        logging.info("Setting %s to %s", key_path, v)
        if zk.exists(key_path):
            zk.set(key_path, "{}".format(v).encode())
        else:
            zk.create(key_path, "{}".format(v).encode(), makepath=True)

    zk.stop()


def flatten_yaml(d, path):
    retval = {}
    for k in d:
        if isinstance(d[k], dict):
            path.append(k)
            retval.update(flatten_yaml(d[k], path))
            path.pop()
        elif isinstance(d[k], list):
            path.append(k)
            for idx, val in enumerate(d[k]):
                retval["{}[{}]".format(".".join(path), idx)] = val
            path.pop()
        else:
            path.append(k)
            retval[".".join(path)] = d[k]
            path.pop()

    return sorted(retval.items())


@click.command()
@click.option("--rm", "is_rm", type=bool, is_flag=True, help="remove nodes before import")
@click.option("-z", "--zookeeper", "zk_url", type=str, help="zookeeper address, if omitted - properties are dumped to stdout")
@click.option("-f", "--filter", "filter", type=str, help="filter prefix, only apply YAML keys starting with prefix")
@click.argument("yaml_file", type=click.File("r"))
@click.argument("root_path", type=str)
def yaml2zk(is_rm, zk_url, filter, yaml_file, root_path):
    """Converts YAML_FILE to flat Java properties-like structure and stores to Zookeeper under ROOT_PATH"""

    data = flatten_yaml(yaml.load(yaml_file), [])

    if filter is not None:
        data = [(k, v) for k, v in data if k.startswith(filter)] 

    if zk_url is None:
        for k, v in data:
            key_path = "{}/{}".format(root_path, k)
            logging.info("Setting %s to %s", key_path, v)
    else:
        save_to_zk(zk_url, root_path, is_rm, data)


if __name__ == "__main__":
    yaml2zk()
