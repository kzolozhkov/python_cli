import click
import sys
import yaml
from kazoo.client import KazooClient


def read_from_zk(zk_url, path):
    zk = KazooClient(hosts=zk_url, read_only=True)
    zk.start()

    retval = None
    if zk.exists(path):
        retval = [(n, zk.get("{}/{}".format(path, n))[0].decode("utf-8")) for n in zk.get_children(path)]

    zk.stop()

    return sorted(retval) if retval is not None else []


@click.command()
@click.option("-z", "--zookeeper", type=str, required=True, help="zookeeper address")
@click.argument("path", type=str)
def zk2yaml(zookeeper, path):
    acc = {}

    for k, v in read_from_zk(zookeeper, path):
        key_arr = k.split(".")
        key_arr_len = len(key_arr)

        if key_arr_len == 2:
            acc[key_arr[0]] = acc.get(key_arr[0], {})
            acc[key_arr[0]][key_arr[1]] = v
        elif key_arr_len == 3:
            acc[key_arr[0]] = acc.get(key_arr[0], {})
            acc[key_arr[0]][key_arr[1]] = acc[key_arr[0]].get(key_arr[1], {})
            acc[key_arr[0]][key_arr[1]][key_arr[2]] = v

    yaml.safe_dump(acc, sys.stdout, default_flow_style=False, encoding='utf-8', allow_unicode=True)


if __name__ == "__main__":
    zk2yaml()
