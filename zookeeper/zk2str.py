import click
from kazoo.client import KazooClient

def read_from_zk(zk_url, path):
    zk = KazooClient(hosts=zk_url, read_only=True)
    zk.start()

    retval = None
    if zk.exists(path):
        retval, _ = zk.get(path)

    zk.stop()
    
    if retval is None:
        raise RuntimeError("{} doesn't exist @ {}".format(path, zk_url))

    return retval.decode('utf-8')

@click.command()
@click.option("-z", "--zookeeper", type=str, required=True, help="zookeeper address")
@click.argument("path", type=str)
def zk2str(zookeeper, path):
    print(read_from_zk(zookeeper, path))


if __name__ == "__main__":
    zk2str()
