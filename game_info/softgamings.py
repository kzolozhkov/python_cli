import csv
import hashlib
import os
import uuid

import click
import requests

url = "https://apitest.fundist.org"
api_key = "71b610dc8a4528f2eb6aa8924c12f98f"
api_password = "8345944474295262"
environment_ip = "0.0.0.0"

game_provider_id_dict = {"973": "Endorphina",
                         "975": "AmaticDirect",
                         "976": "Habanero",
                         "983": "Ezugi",
                         "984": "Playson",
                         "987": "TomHorn",
                         "991": "BetSoft",
                         "992": "NetEnt",
                         "997": "Microgaming",
                         "998": "Evolution"}

game_auth_id_dict = {"973": "DirectAuth",
                     "975": "DirectAuth",
                     "976": "DirectAuth",
                     "983": "DirectAuth",
                     "984": "DirectAuth",
                     "987": "AuthHTML",
                     "991": "DirectAuth",
                     "992": "AuthHTML",
                     "997": "DirectAuth",
                     "998": "DirectAuth"}

headers = ['Legal Game Name (fullGameName)', 'Game ID (gameId)', 'Game Type (gameType)',
           'Game Provider ID(gameProviderId)', 'Aggregator ID(aggregatorId)', 'Type(game_info_type)',
           'Start Game Url(startGameUrl)', 'Stop Game Url(stopGameUrl)', 'Lines(lines)', 'ModuleId(module_id)',
           'ClientId(client_id)', 'Coins(coins)', 'CoinSizes(coin_sizes)', 'BetLevel(bet_level)',
           'CoinValueLevel(coin_value_level)',
           'FunGameUrl(start_fun_game_url)', 'FunModeSupported(fun_mode_supported)',
           'FreeSpinSupported(free_spin_supported)', 'Disabled(disabled)']


@click.command()
@click.option("-d", "--destination", type=str, required=True,
              help="Enter either 'Documents' or 'Downloads', or any other destination is User's home directory")
def create_softgamings_games_csv(destination):
    softgamings_game_json_data = get_softgamings_games_json()
    categories_dict = create_dict_from_categories(get_softgamings_categories_json())

    path = os.path.join(os.path.expanduser('~'), destination, 'softgamings-games.csv')
    softgamings_csv_file = open(path, 'w')

    # Write header to the CSV
    csvwriter = csv.writer(softgamings_csv_file)
    csvwriter.writerow(headers)

    for el in softgamings_game_json_data:

        # We shouldn't process element if its systemId == 993 since we don't support BetSoft
        if el['System'] == '993':
            continue

        # Building column values here
        full_game_name = el['Trans']['en']
        game_id = el['ID']
        game_type = get_game_type(el, categories_dict)
        game_provider_id = game_provider_id_dict[el['System']].lower()
        aggregator_id = 'softgamings'
        game_info_type = get_game_info_type(el)
        start_game_url = get_start_game_url(el, game_id, game_info_type, game_provider_id, aggregator_id)
        stop_game_url = '/softgamings/stop_game'
        lines = ''
        module_id = ''
        client_id = ''
        coins = ''
        coin_sizes = ''
        bet_level = get_bet_level(el)
        coin_value_level = ''
        start_fun_game_url = get_start_fun_game_url(el, game_id, game_info_type)
        fun_mode_supported = get_fun_mode_supported(el)
        free_spin_supported = get_free_spin_supported(el)
        disabled = 'False'

        # Aggregating everything into one row
        csv_row = [full_game_name,
                   game_id, game_type,
                   game_provider_id,
                   aggregator_id,
                   game_info_type,
                   start_game_url,
                   stop_game_url,
                   lines, module_id,
                   client_id,
                   coins,
                   coin_sizes,
                   bet_level,
                   coin_value_level,
                   start_fun_game_url,
                   fun_mode_supported,
                   free_spin_supported,
                   disabled]

        csvwriter.writerow(csv_row)
    softgamings_csv_file.close()


def get_softgamings_games_json():
    tid = uuid.uuid4()
    game_list_hash_url = "Game/List/{}/{}/{}/{}".format(environment_ip, tid, api_key, api_password)
    encoded_game_list_url = game_list_hash_url.encode('utf-8')
    md5_game_list_hash = hashlib.md5(encoded_game_list_url).hexdigest()

    game_list_url = "{}/System/Api/{}/Game/List?&TID={}&Hash={}".format(url, api_key, tid, md5_game_list_hash)
    resp_games = requests.get(game_list_url)
    resp_games.raise_for_status()

    return resp_games.json()


def get_softgamings_categories_json():
    tid = uuid.uuid4()
    game_categories_hash_url = "Game/Categories/{}/{}/{}/{}".format(environment_ip, tid, api_key, api_password)
    encoded_game_catergories_url = game_categories_hash_url.encode('utf-8')
    md5_game_categories_hash = hashlib.md5(encoded_game_catergories_url).hexdigest()

    game_categories_url = "{}/System/Api/{}/Game/Categories/?&TID={}&Hash={}".format(url, api_key, tid,
                                                                                     md5_game_categories_hash)
    resp_categories = requests.get(game_categories_url)
    resp_categories.raise_for_status()

    return resp_categories.json()


def get_game_type(el, categories_dict):
    list_of_categories = el['Categories']
    if len(list_of_categories) == 1:
        return categories_dict[list_of_categories[0]]
    else:
        list_of_category_names = [categories_dict[k] for k in list_of_categories]
        if 'Live Casino' in list_of_category_names:
            return 'Live Casino'
        else:
            category_name = filter_list_of_category_names(list_of_category_names, "New")
            if category_name is not None:
                return category_name
            category_name = filter_list_of_category_names(list_of_category_names, "Popular")
            if category_name is not None:
                return category_name
            category_name = filter_list_of_category_names(list_of_category_names, "Main Page")
            if category_name is not None:
                return category_name
            category_name = filter_list_of_category_names(list_of_category_names, "Table Games")
            if category_name is not None:
                return category_name
            else:
                return list_of_category_names[0]


def filter_list_of_category_names(list_of_category_names, category_name):
    if category_name in list_of_category_names:
        list_of_category_names.remove(category_name)
        if len(list_of_category_names) == 1:
            return list_of_category_names[0]
    else:
        return None


def get_game_info_type(el):
    if el['PageCode'] is not None and el['MobilePageCode'] is not None:
        return 'DESKTOP_AND_MOBILE'
    elif el['MobilePageCode'] is None:
        return 'DESKTOP'
    else:
        return 'MOBILE'


def get_bet_level(el):
    if game_provider_id_dict[el['System']] == 'NetEnt' and 'Freeround' in el and el['Freeround'] == '1':
        return '1-10'
    else:
        return ''


def get_free_spin_supported(el):
    if 'Freeround' in el and el['Freeround'] == '1':
        return True
    else:
        return False


def get_fun_mode_supported(el):
    if el['HasDemo'] == "1":
        return True
    else:
        return False


def get_start_game_url(el, game_id, game_info_type, game_provider_id, aggregator_id):
    return "/softgamings/start_game?gameId={}&gameProviderId=softgamings&systemId={}&authType={}&{}&internalGameId={}" \
        .format(game_id, el['System'], game_auth_id_dict[el['System']],
                get_page_code_query_parameters(el, game_info_type),
                get_internal_game_id(game_id, game_info_type, game_provider_id, aggregator_id))


def get_start_fun_game_url(el, game_id, game_info_type):
    if get_fun_mode_supported(el):
        return "/softgamings/public/start_game?gameId={}&gameProviderId=softgamings&systemId={}&authType={}&{}" \
            .format(game_id, el['System'], game_auth_id_dict[el['System']],
                    get_page_code_query_parameters(el, game_info_type))
    else:
        return ''


def get_page_code_query_parameters(el, game_info_type):
    if game_info_type == 'DESKTOP_AND_MOBILE':
        return "pageCode={}&mobilePageCode={}".format(el['PageCode'], el['MobilePageCode'])
    elif game_info_type == 'DESKTOP':
        return "pageCode={}".format(el['PageCode'])
    else:
        return "mobilePageCode={}".format(el['MobilePageCode'])


def get_internal_game_id(game_id, game_info_type, game_provider_id, aggregator_id):
    return "{}:{}:{}:{}".format(game_id, game_info_type, game_provider_id, aggregator_id)


def create_dict_from_categories(parsed_categories_json_data):
    categories_dict = {}
    for category in parsed_categories_json_data:
        category_key = category["ID"]
        category_value = category['Trans']['en']
        categories_dict.update({category_key: category_value})
    return categories_dict


if __name__ == "__main__":
    create_softgamings_games_csv()
