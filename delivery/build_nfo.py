import click
import datetime
import logging

from delivery.docker_utils import docker_build_nfo, docker_tag_and_push, devregistry_ref, services_from_env

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)

LATEST_TAG = 'latest'


@click.command()
@click.option("--e2e-digest", "e2e", type=str, envvar='NAS_E2E_DIGEST', default=None)
@click.option("--ui-digest", "ui", type=str, envvar='NAS_UI_DIGEST', default=None)
def build_nfo(e2e, ui):
    if e2e is None or ui is None:
        logging.warning("Skip building NFO, E2E/UI digest is missing, E2E=%s, UI=%s", e2e, ui)
        return

    now = datetime.datetime.today()

    nfo = {
        "platform": {
            "creation_date": "{}".format(now), "creation_date_kyiv": "{}".format(now)
        },
        "services": services_from_env("hrzn01_dev"),
        "tests": {
            "e2e": {"local_repo_digest": e2e, 'release_repo_digest': None},
            "ui": {"local_repo_digest": ui, 'release_repo_digest': None}
        }
    }

    logging.info("Tests: %s", nfo["tests"])

    docker_build_nfo(nfo, devregistry_ref("nfo"))
    docker_tag_and_push(None, devregistry_ref("nfo"))


if __name__ == "__main__":
    build_nfo()
