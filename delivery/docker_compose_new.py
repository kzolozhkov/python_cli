import io
import os
from pathlib import Path
from urllib.parse import urlparse

import click
from deepmerge import always_merger
from jinja2 import Environment, PackageLoader
from ruamel.yaml import YAML
from sys import stdout

from delivery.docker_utils import registry_ref_new, secrets_from_env_new

yaml = YAML()
yaml.indent(offset=2, sequence=4)

DEFAULT_JAVA_OPTS = "-Xloggc:/var/log/nas/{}_$${{HOSTNAME}}_gc.log -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintTenuringDistribution -XX:+PrintGCCause -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=2M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/var/log/nas/"


def to_nice_yaml(v):
    output = io.StringIO()
    yaml.dump(v, output)
    return output.getvalue()


def create_default(nas_project, nas_project_config, nfo):
    config_logstash_url = urlparse(nas_project_config["logstash"]["url"]).hostname
    return {
        "environment": {
            "NAS_PROJECT": nas_project,
            "LOGSTASH_URL": config_logstash_url,
            "GRAYLOG_HOST": config_logstash_url
        },
        "logging": {"driver": "json-file", "options": {"max-size": "20m", "max-file": "1"}},
        "networks": ["env_network"],
        "healthcheck": {
            "interval" : "45s"
        }
    }


def service_generator(services, secrets, nas_project, nas_project_config, nfo):
    for srv_name, common_path, override_path in services:
        retval = {srv_name: create_default(nas_project, nas_project_config, nfo)}

        with common_path.open("r") as s:
            always_merger.merge(retval, yaml.load(s))

        if srv_name == "gateway":
            retval[srv_name]["environment"]["NAS_VERSION"] = "latest" if nfo is None else nfo["platform"]["creation_date"]

        retval[srv_name]["environment"]["NAS_SERVICE"] = srv_name

        if "JAVA_OPTS" in retval[srv_name]["environment"]:
            retval[srv_name]["environment"]["JAVA_OPTS"] = "{} {}".format(retval[srv_name]["environment"]["JAVA_OPTS"],
                                                                          DEFAULT_JAVA_OPTS.format(srv_name))

        retval[srv_name]["volumes"] = [
            "/var/log/nas:/var/log/nas",
            "/opt/hrzn/config/application-{}.yml:/{}/lib/etc/application-{}.yml".format(
                retval[srv_name]["environment"]["NAS_PROJECT"], srv_name,
                retval[srv_name]["environment"]["NAS_PROJECT"]),
            "/opt/hrzn/config/{}:/{}/lib/etc".format(srv_name, srv_name)
        ]

        if srv_name in secrets:
            retval[srv_name]["secrets"] = [{
                "source": secrets[srv_name], "target": "/{}/lib/etc/application-secret.yml".format(srv_name)
            }]

        if override_path.exists():
            with override_path.open() as s:
                always_merger.merge(retval, yaml.load(s))

        if nfo is None:
            retval[srv_name]["image"] = registry_ref_new(retval[srv_name]["image"], dev=True)
        else:
            if srv_name in nfo["services"]:
                retval[srv_name]["image"] = nfo["services"][srv_name]['release_repo_digest']
            else:
                raise RuntimeError("{} is not found in NFO".format(srv_name))

        yield retval


@click.command()
@click.option("-N", "--nfo", "nfo_file", type=click.File("r"))
@click.argument('nas_project', envvar='NAS_PROJECT')
def docker_compose(nfo_file, nas_project):
    cfg_root = Path("/opt/hrzn/config") if os.environ[
                                               'USER'] == 'jenkins' else Path.home() / ".config" / "hrzn" / nas_project

    nas_project_config = yaml.load(cfg_root / "application-{}.yml".format(nas_project))

    nfo = None if nfo_file is None else yaml.load(nfo_file)

    secrets = secrets_from_env_new(nas_project)

    services = [(srv_name, cfg_root / srv_name / "docker_compose.yml") for srv_name in
                sorted(nas_project_config["hrzn"]["services"].keys())]

    services = [(srv_name, p, cfg_root / srv_name / "docker_compose.override.yml") for srv_name, p in services if
                p.exists()]

    j2_env = Environment(loader=PackageLoader("delivery", "templates"))
    j2_env.filters['to_nice_yaml'] = to_nice_yaml
    j2_env.get_template("docker_compose_new.j2").stream(
        nas_project=nas_project,
        services=service_generator(services, secrets, nas_project, nas_project_config, nfo),
        secrets=secrets
    ).dump(stdout)


if __name__ == "__main__":
    docker_compose()
