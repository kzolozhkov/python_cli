import click
import logging
import sys
import yaml

import delivery.docker_utils

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)


@click.command()
def nfo_from_docker():
    yaml.safe_dump(delivery.docker_utils.nfo_from_docker(delivery.docker_utils.registry_ref('nfo')),
                   sys.stdout, default_flow_style=False, encoding='utf-8', allow_unicode=True)


if __name__ == "__main__":
    nfo_from_docker()
