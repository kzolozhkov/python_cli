import json
import logging

import click
import requests
import yaml
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util import Retry

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)


def do_check(session, url):
    logging.info("HEALTHCHECK [ %s ]", url)
    r = session.get(url, timeout=(3, 3))
    retval = r.json()
    logging.info("JSON = %s", json.dumps(retval, indent=2))
    return retval


def healcheck_urls(config):
    api_url = config['hrzn']['api_url']
    for s in sorted(config['hrzn']['services']):
        if s == "website":
            yield "{}/health".format(api_url.replace('api', 'site'))
        elif s == "cms":
            yield "{}/health".format(api_url.replace('api', 'cms'))
        elif s == "backoffice":
            yield "{}/health".format(api_url.replace('api', 'backoffice'))
        elif s == "gateway":
            yield "{}/health".format(api_url)
        else:
            yield "{}/{}/health".format(api_url, s)


@click.command()
@click.argument("cfg", type=click.File("r"))
def healthcheck(cfg):
    session = requests.Session()
    adapter = HTTPAdapter(max_retries=Retry(connect=3, read=5, status=5, backoff_factor=10, raise_on_status=True,
                                            status_forcelist=[500, 502]))
    session.mount("http://", adapter)
    session.mount("https://", adapter)

    config = yaml.load(cfg)

    for s in healcheck_urls(config):
        do_check(session, s)

    do_check(session, "{}/subjects".format(config["kafka"]["schema"]["registry"]["url"]))


if __name__ == "__main__":
    healthcheck()
