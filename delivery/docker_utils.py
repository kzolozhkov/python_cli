import json
import logging

import docker
import yaml
from docker.errors import NotFound, ImageNotFound

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)

client = docker.from_env(timeout=120)
client_low = docker.APIClient(base_url='unix://var/run/docker.sock')


def docker_short_digest(repo_digest):
    arr = repo_digest.split("@") if repo_digest is not None else []
    if len(arr) > 1:
        return arr[1].replace("sha256:", "")[:12]
    else:
        return repo_digest


def docker_rm_service(srv_name, stack):
    try:
        srv = client.services.get("{}_{}".format(stack, srv_name))
    except NotFound:
        pass
    else:
        srv.remove()


def docker_tag_and_push(src, dest):
    if src is not None:
        logging.info("Tagging %s with %s ... ", src, dest)
        client_low.tag(src, dest)
        logging.info("DONE")

    logging.info("Pushing %s ... ", dest)

    try:
        out = client.images.push(dest)
    except Exception as e:
        logging.error("ERROR pushing %s", dest)
        raise e

    if "errorDetail" in out:
        raise RuntimeError(out)
    else:
        logging.info("DONE")
        return json.loads(out.splitlines()[-1])["aux"]


def docker_build_nfo(nfo, repo_tag):
    logging.info("Building NFO %s ... ", repo_tag)
    with open("nfo.yml", "w") as f:
        yaml.safe_dump(nfo, f, default_flow_style=False, encoding='utf-8', allow_unicode=True)
    img = client.images.build(path=".", tag=repo_tag, rm=True, dockerfile="Dockerfile.nfo")
    logging.info("DONE %s", img.attrs['Id'])


def registry_ref_new(service, digest=None, dev=False):
    registry = "devregistry.newage.io" if dev else "registry.newage.io"

    if digest is not None:
        return "{}/{}@{}".format(registry, service, digest)
    else:
        return "{}/{}:latest".format(registry, service)


def devregistry_ref(srv, digest=None, custom=None):
    ns = "hrzn/{}".format(srv) if custom is None else custom

    if digest is not None:
        return "devregistry.newage.io/{}@{}".format(ns, digest)
    else:
        return "devregistry.newage.io/{}:latest".format(ns)


def registry_ref(srv, digest=None, custom=None):
    ns = "hrzn/{}".format(srv) if custom is None else custom

    if digest is not None:
        return "registry.newage.io/{}@{}".format(ns, digest)
    else:
        return "registry.newage.io/{}:latest".format(ns)


def nfo_from_docker(ref):
    try:
        client.images.pull(ref)
    except (NotFound, ImageNotFound):
        return {}

    out = client.containers.run(ref, remove=True)
    return yaml.load(out)


def services_from_env(stack):
    retval = {}
    service_specs = [s.attrs['Spec'] for s in client.services.list(filters={'name': stack})]
    service_specs = [s for s in service_specs if s.get('Labels', {}).get('com.docker.stack.namespace', None) == stack]
    for s in service_specs:
        service_name = s['Name'].replace(s["Labels"]["com.docker.stack.namespace"] + "_", "")
        local_repo_digest = s['TaskTemplate']['ContainerSpec']['Image']
        retval[service_name] = {"local_repo_digest": local_repo_digest}
    return retval


def secrets_from_env(stack):
    return [s.name.replace(stack + "_", "").replace("_secret", "") for s in client.secrets.list()]


def secrets_from_env_new(nas_project):
    return {s.name.replace(nas_project + "_", "").replace("_secret", ""): s.name for s in client.secrets.list()}


def swarm_workers():
    return client.nodes.list(filters={'role': 'worker'})
