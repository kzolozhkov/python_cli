import click
import logging
from docker.errors import ImageNotFound

from delivery.docker_utils import devregistry_ref, registry_ref, client, nfo_from_docker, docker_tag_and_push, \
    docker_short_digest, docker_build_nfo

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)

LATEST_TAG = 'latest'


@click.command()
def release():
    try:
        client.images.remove(registry_ref("nfo"))
    except ImageNotFound:
        pass

    devregistry_nfo = nfo_from_docker(devregistry_ref("nfo"))
    registry_nfo = nfo_from_docker(registry_ref("nfo"))

    if ('release_repo_digest' not in registry_nfo.get('tests', {}).get('e2e', {}) or
            devregistry_nfo['tests']['e2e']['local_repo_digest'] != registry_nfo['tests']['e2e'][
                'local_repo_digest']):

        client.images.pull(devregistry_nfo['tests']['e2e']['local_repo_digest'])

        out = docker_tag_and_push(devregistry_nfo['tests']['e2e']['local_repo_digest'],
                                  registry_ref('e2e_tests'))
        devregistry_nfo["tests"]['e2e']["release_repo_digest"] = registry_ref('e2e_tests', digest=out["Digest"])
    else:
        devregistry_nfo['tests']['e2e']['release_repo_digest'] = registry_nfo['tests']['e2e']['release_repo_digest']

    if ('release_repo_digest' not in registry_nfo.get('tests', {}).get('ui', {}) or
            devregistry_nfo['tests']['ui']['local_repo_digest'] != registry_nfo['tests']['ui'][
                'local_repo_digest']):

        client.images.pull(devregistry_nfo['tests']['ui']['local_repo_digest'])

        out = docker_tag_and_push(devregistry_nfo['tests']['ui']['local_repo_digest'],
                                  registry_ref('ui_tests'))
        devregistry_nfo["tests"]['ui']["release_repo_digest"] = registry_ref('ui_tests', digest=out["Digest"])
    else:
        devregistry_nfo['tests']['ui']['release_repo_digest'] = registry_nfo['tests']['ui']['release_repo_digest']

    rel_services_nfo = registry_nfo.get("services", {})

    for srv, srv_nfo in sorted(devregistry_nfo["services"].items()):
        logging.info("[ %s ]", srv)
        logging.info("local=%s, devregistry=%s, registry=%s",
                     docker_short_digest(srv_nfo["local_repo_digest"]),
                     docker_short_digest(rel_services_nfo.get(srv, {}).get("local_repo_digest")),
                     docker_short_digest(rel_services_nfo.get(srv, {}).get("release_repo_digest")))

        if (srv not in rel_services_nfo or
                "release_repo_digest" not in rel_services_nfo[srv] or
                srv_nfo["local_repo_digest"] != rel_services_nfo[srv].get("local_repo_digest")):

            client.images.pull(srv_nfo["local_repo_digest"])

            out = docker_tag_and_push(srv_nfo["local_repo_digest"],
                                      registry_ref(srv))
            devregistry_nfo["services"][srv]["release_repo_digest"] = registry_ref(srv, digest=out["Digest"])
        elif (srv_nfo["local_repo_digest"] == rel_services_nfo[srv].get("local_repo_digest") and
              "release_repo_digest" in rel_services_nfo[srv]):
            devregistry_nfo["services"][srv]["release_repo_digest"] = rel_services_nfo[srv]["release_repo_digest"]

    docker_build_nfo(devregistry_nfo, registry_ref("nfo"))
    docker_tag_and_push(None, registry_ref("nfo"))

    logging.info("Successfully released HRZN platform [%s]", devregistry_nfo["platform"]["creation_date"])


if __name__ == "__main__":
    release()
