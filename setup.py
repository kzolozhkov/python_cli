from setuptools import setup, find_packages

setup(
    name='hrzn-cli',

    version='1.0',

    description='HRZN iGaming platform command line utilities.',

    # The project's main homepage.
    url='https://bitbucket.org/nasio/casino_cli',

    # Author details
    author='New Age Sol.',
    author_email='dev@newagesol.com',

    classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python :: 3.6',
    ],

    install_requires=['tabulate', 'prompt_toolkit==1.0.15',
                      'parsimonious', 'kazoo', 'pyaml', 'docker==2.7.0',
                      'click==6.7', 'pygments', 'Jinja2==2.10', 'ruamel.yaml', 'deepmerge',
                      'httpie', "httpie-jwt-auth", "xdg==3.0.1", "validators"
                      ],

    python_requires='>=3.6',

    packages=find_packages(),

    package_data={'delivery': ['templates/*.j2']},

    entry_points={
        'console_scripts': [
            'hrzn                       =   hrzn_cli.cli:hrzn_cli',

            'yaml2zk                    =   zookeeper.yaml2zk:yaml2zk',
            'zk2str                     =   zookeeper.zk2str:zk2str',
            'zk2yaml                    =   zookeeper.zk2yaml:zk2yaml',

            'healthcheck                =   delivery.healthcheck:healthcheck',
            'build_nfo                  =   delivery.build_nfo:build_nfo',
            'release                    =   delivery.release:release',
            'docker_compose             =   delivery.docker_compose_new:docker_compose',
            'docker_compose_new         =   delivery.docker_compose_new:docker_compose',
            'nfo_from_docker            =   delivery.nfo_from_docker:nfo_from_docker',
        ],
    },
)
